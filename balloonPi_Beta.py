import json
import os
import envirophat as enviro
from picamera import PiCamera
import time
import iso8610
import threading
from gps3.agps3threaded import AGPS3mechanism

def tempProbe():
    temp_probe = envriophat.analog.read(0)
    t_temp_probe = str(round(temp_probe * 100 - 50, 2))
    return temp_probe, t_temp_probe

def getGPS():
    agps_thread = AGPS3mechanism()  # Instantiate AGPS3 Mechanisms
    agps_thread.stream_data()  # From localhost (), or other hosts, by example, (host='gps.ddns.net')
    agps_thread.run_thread()  # Throttle time to sleep after an empty lookup, default '()' 0.2 two tenths of a second
    
    while True:
        print('---------------------')
        print(                   agps_thread.data_stream.time)
        print('Lat:{}   '.format(agps_thread.data_stream.lat))
        print('Lon:{}   '.format(agps_thread.data_stream.lon))
        print('Altitude:{}'.format(agps_thread.data_stream.alt))
        print('Speed:{} '.format(agps_thread.data_stream.speed))
        print('Course:{}'.format(agps_thread.data_stream.track))
        print('---------------------')
    
    lat = agps_thread.data_stream.lat
    lon = agps_thread.data_stream.lon
    speed = agps_thread.data_stream.speed
    heading = agps_thread.data_stream.track
    gpsTime = agps_thread.data_stream.time
    time = iso8601.parse_date(gpsTime)
    alt = agps_thread.data_stream.alt
    return lat, lon, speed, heading, gpsTime, time, alt
        
def getData():
    pressure = weather.pressure()
    temp = weather.temperature()
    magnetometer = motion.magnetometer()
    accelorometer = motion.accelerometer()
    e_heading = motion.heading()
    e_raw_heading = motion.raw_heading()
    temp_probe, t_temp_probe = tempProbe()
    lat, lon, speed, heading, gpsTime, time = getGPS()
    return pressure, temp, magnetometer, accelorometer, e_heading, e_raw_heading, temp_probe, t_temp_probe, lat, lon, speed, heading, gpsTime, time, alt

def writeImg():
    imageTaken = "N"
    camera = PiCamera()
    camera.resolution = (2592, 1944)
    camera.start_preview()
    sleep(2)
    pressure, temp, magnetometer, accelorometer, e_heading, e_raw_heading, temp_probe, t_temp_probe, lat, lon, speed, heading, gpsTime, time, alt = getData()
    camera.annotate_text = 'Time: ' + time + ' Altitude: ' + alt
    camera.capture('./' + gpsTime + '/1.jpg')
    imageTaken = "Y"
    return imageTaken

def writeJson():
    
    imageTaken = writeImg()
    pressure, temp, magnetometer, accelorometer, e_heading, e_raw_heading, temp_probe, t_temp_probe, lat, lon, speed, heading, gpsTime, time, alt = getData()

    with open('./' + gpsTime + '/data.json') as r:
        oldData = json.load(r)
    
    newData[time] = {}
    newData[time]["Latitude"] = lat
    newData[time]["Longitude"] = lon
    newData[time]["Altitude"] = alt
    newData[time]["Speed"] = speed
    newData[time]["Heading"] = heading
    newData[time]["Temp"] = t_temp_probe
    newData[time]["Accelorometer"] = accelorometer
    newData[time]["Magnetometer"] = magnetometer
    newData[time]["Pressure"] = pressure
    newData[time]["Photo Taken"] = imageTaken
        
    with open('./' + gpsTime + '/data.json', 'w') as w:
        json.dump(newData, w)
        
    with open('./' + gpsTime + '/data.json', 'w') as w:
        json.dump(newData, w)
    
def write():
    writeJson()
    #writeImg()
    
### RUN CODE ###
while True:
    write()
    sleep(30)
    