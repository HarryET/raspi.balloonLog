import json
import os
import envirophat as enviro
from picamera import PiCamera
import time
import iso8610
import threading
from gps3.agps3threaded import AGPS3mechanism

## Get Temp Probe Data ##

def tempProbe():
    temp_probe = envriophat.analog.read(0)
    t_temp_probe = str(round(temp_probe * 100 - 50, 2))
    return temp_probe, t_temp_probe
    
## Get GPS Data ##

def getGPS():
    agps_thread = AGPS3mechanism()  # Instantiate AGPS3 Mechanisms
    agps_thread.stream_data()  # From localhost (), or other hosts, by example, (host='gps.ddns.net')
    agps_thread.run_thread()  # Throttle time to sleep after an empty lookup, default '()' 0.2 two tenths of a second
    
    while True:
        print('---------------------')
        print(                   agps_thread.data_stream.time)
        print('Lat:{}   '.format(agps_thread.data_stream.lat))
        print('Lon:{}   '.format(agps_thread.data_stream.lon))
        print('Altitude:{}'.format(agps_thread.data_stream.alt))
        print('Speed:{} '.format(agps_thread.data_stream.speed))
        print('Course:{}'.format(agps_thread.data_stream.track))
        print('---------------------')
    
    lat = agps_thread.data_stream.lat
    lon = agps_thread.data_stream.lon
    speed = agps_thread.data_stream.speed
    heading = agps_thread.data_stream.track
    alt = agps_thread.data_stream.alt
    return lat, lon, speed, heading, alt
    
## Get Time ##

def getTime():
    agps_thread = AGPS3mechanism()  # Instantiate AGPS3 Mechanisms
    agps_thread.stream_data()  # From localhost (), or other hosts, by example, (host='gps.ddns.net')
    agps_thread.run_thread()  # Throttle time to sleep after an empty lookup, default '()' 0.2 two tenths of a second 
    gpsTime = agps_thread.data_stream.time # Get Time From Stream
    time = iso8601.parse_date(gpsTime) # Convert Time
    return time, gpsTime
 
## Get Enviro Phat Data ##
    
def getData():
    pressure = weather.pressure()
    temp = weather.temperature()
    magnetometer = motion.magnetometer()
    accelorometer = motion.accelerometer()
    e_heading = motion.heading()
    e_raw_heading = motion.raw_heading()
    return pressure, temp, magnetometer, accelorometer, e_heading, e_raw_heading
    
## Write Data ##

def writeData():
    while True:
        imageTaken = "N"
        temp_probe, t_temp_probe = tempProbe()
        lat, lon, speed, heading, alt = getGPS()
        time, gpsTime = getTime()
        pressure, temp, magnetometer, accelorometer, e_heading, e_raw_heading = getData()
        camera = PiCamera()
        camera.resolution = (2592, 1944)
        camera.start_preview()
        sleep(2)
        camera.annotate_text = 'Time: ' + time + ' Altitude: ' + alt
        camera.capture('./init.jpg')
        imageTaken = "Y"
        
            with open('./' + gpsTime + '/data.json') as r:
            oldData = json.load(r)
        
        newData[time] = {}
        newData[time]["Latitude"] = lat
        newData[time]["Longitude"] = lon
        newData[time]["Altitude"] = alt
        newData[time]["Speed"] = speed
        newData[time]["Heading"] = heading
        newData[time]["Temp"] = t_temp_probe
        newData[time]["Accelorometer"] = accelorometer
        newData[time]["Magnetometer"] = magnetometer
        newData[time]["Pressure"] = pressure
        newData[time]["Photo Taken"] = imageTaken
            
        with open('.data.json', 'w') as w:
            json.dump(newData, w)
            
        with open('./' + gpsTime + '/data.json', 'w') as w:
            json.dump(newData, w)
            
        sleep(10)
        
## Run Code

writeData()


### Made By Harry Bairstow ###
    